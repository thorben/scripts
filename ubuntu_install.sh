USERNAME=$(whoami)
USERHOME=$HOME

# install git
sudo apt-get -y install git

# install zsh
sudo apt-get -y install zsh

# install oh-my-zsh
if [ -d "$USERHOME/.oh-my-zsh" ]; then
	git -C $USERHOME/.oh-my-zsh pull
fi
if [ ! -d "$USERHOME/.oh-my-zsh" ]; then
	git clone git://github.com/robbyrussell/oh-my-zsh.git $USERHOME/.oh-my-zsh
fi
rm -f $USERHOME/.zshrc
cp $USERHOME/.oh-my-zsh/templates/zshrc.zsh-template $USERHOME/.zshrc
sudo chsh -s /bin/zsh $USERNAME
